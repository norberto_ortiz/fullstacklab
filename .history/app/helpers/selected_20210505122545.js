import { helper } from '@ember/component/helper';

export default helper((aStr) => return String(aStr).padStart(3, '0'));
